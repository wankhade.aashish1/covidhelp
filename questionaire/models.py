from django.db import models

# Create your models here.

class UserDetail(models.Model):
    fullname = models.CharField(max_length=50, null=False,blank=False)
    contact = models.CharField(max_length=15)
    email = models.EmailField()

class Question(models.Model):
    question = models.CharField(max_length=300, null=False, blank=False)
    answer = models.CharField(max_length=50)
    points = models.IntegerField()

class UserResponse(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='question_resp')
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE, related_name='user_resp')
    user_ans = models.CharField(max_length=50)
    user_point = models.IntegerField(null=True, default=0)
