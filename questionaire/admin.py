from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(UserDetail)
class UserDetailsAdmin(admin.ModelAdmin):
    list_display = ['fullname','contact','email']

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question','answer','points']

@admin.register(UserResponse)
class UserResponseAdmin(admin.ModelAdmin):
    list_display = ['get_question','get_user', 'user_ans','user_point']

    def get_question(self, obj):
        return obj.question.question
    get_question.admin_order_field = 'question'
    get_question.short_description = 'Question'

    def get_user(self, obj):
        return obj.user.fullname
    get_user.admin_order_field = 'fullname'
    get_user.short_description = 'Fullname'