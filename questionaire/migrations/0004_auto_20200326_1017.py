# Generated by Django 2.2 on 2020-03-26 10:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('questionaire', '0003_auto_20200326_1003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userresponse',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='question_resp', to='questionaire.Question'),
        ),
        migrations.AlterField(
            model_name='userresponse',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_resp', to='questionaire.UserDetail'),
        ),
    ]
