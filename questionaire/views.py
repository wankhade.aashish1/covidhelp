from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from .models import *
from django.views.generic.edit import CreateView
# Create your views here.

class UserCreateView(CreateView):
    model = UserDetail
    fields = "__all__"
    template_name = 'questionaire/covidstep1.html'
    success_url = reverse_lazy('show-questions')

    def get_form(self,form_class=None):
        if form_class is None:
            form_class = self.get_form_class()        
        form = super(UserCreateView,self).get_form(form_class)
        form.fields['fullname'].widget.attrs = {'class':'form-control','placeholder':'Enter Full Name'}
        form.fields['contact'].widget.attrs = {'class':'form-control','placeholder':'Preferably Whatsapp'}
        form.fields['email'].widget.attrs = {'class':'form-control','placeholder':'name@example.com'}
        return form
    
    def form_valid(self,form):
        form_ret = super().form_valid(form)
        user_details = self.request.session.get('user_id')
        if user_details:
            del self.request.session['user_id']
        self.request.session['user_id']=form.instance.id
        return form_ret

def  newUser(request):
    user_details = request.session.get('user_id')
    if not user_details:
        return redirect('/questionaire/')
    try:
        del request.session['user_id']
    except:
        pass
    return redirect('/questionaire/')

def questionaire(request):
    user_details = request.session.get('user_id')
    if not user_details:
        return redirect('/questionaire/')
    user_details = int(user_details)
    user = UserDetail.objects.get(pk=user_details)
    if request.method == 'GET':
        question_set = Question.objects.all()
        return render(request, 'questionaire/questions.html',{'questionset':question_set, 'fullname':user.fullname})
    if request.method == 'POST':
        question_set = Question.objects.all()
        points_scored = 0
        for question in question_set:
            key = "question_"+str(question.id)
            resp = request.POST.get(key)
            userpoint=0
            if question.answer == str(resp):
                userpoint = question.points
            points_scored+=userpoint
            user_resp, created = UserResponse.objects.get_or_create(question=question,user=user)
            user_resp.user_ans = resp
            user_resp.user_point = userpoint
            user_resp.save()
        return render(request, 'questionaire/questions.html',{'questionset':question_set, 'points_scored':points_scored, 'fullname':user.fullname})
