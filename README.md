# CovidHelp

**Requirements:**
1. To develop the interface 
2. Questionaire for symtoms and connect it with feedback to track and predict stage of infection
3. Connect the skeptical cases, ctype flyers, and positive cases with gps locator
4. Integrated panchayat contact to connect cases with health department

**Technology Stack for development:**
1. Django for backend
2. Bootstrap for frontend

**Steps to get started with for contributing for this repo:**

## Install Required Tools

### Install [Git](https://git-scm.com/download/linux)

```bash
sudo apt install git
```
### Clone the repository
git clone https://gitlab.com/wankhade.aashish1/covidhelp.git

cd covidhelp

### Creating a virtual environment

# On macOS and Linux:
```bash
python3 -m venv env
```
# On Windows
```bash
py -m venv env
```

### Install requirements
pip install -r requirements.txt

### Run on local system
./manage.py runserver 